FROM library/ubuntu:17.10
MAINTAINER Florian Thöni <florian.thoni@floth.fr>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update; \
   apt-get install -y texlive-full; \
   apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
